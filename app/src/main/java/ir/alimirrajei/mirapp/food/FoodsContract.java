package ir.alimirrajei.mirapp.food;

import java.util.ArrayList;

public class FoodsContract {

    interface View extends BaseView<Presenter> {

        void showLoading();

        void hideLoading();

        void showError();

        void showData(ArrayList<Food> foods);

        void showErrorWithText(String text);
    }

    interface Presenter extends BasePresenter<View, Food> {

        void getFoods();

    }
}
