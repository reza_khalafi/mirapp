package ir.alimirrajei.mirapp.food;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ir.alimirrajei.mirapp.R;

public class FoodsAdapter extends RecyclerView.Adapter<FoodsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Food> foods;
    private FoodsInterface foodsInterface;

    public FoodsAdapter(Context context, FoodsInterface foodsInterface) {
        this.context = context;
        this.foodsInterface = foodsInterface;
    }

    public void setData(ArrayList<Food> foods) {
        this.foods = foods;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Food food = foods.get(position);
        holder.tvTitle.setText(food.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodsInterface.itemClicked(food);
            }
        });

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodsInterface.delete(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return foods != null ? foods.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView tvTitle;
        AppCompatTextView tvDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDelete = itemView.findViewById(R.id.tvDelete);
        }
    }
}
