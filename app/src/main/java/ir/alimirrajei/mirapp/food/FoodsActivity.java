package ir.alimirrajei.mirapp.food;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

import ir.alimirrajei.mirapp.R;

public class FoodsActivity extends AppCompatActivity implements FoodsContract.View {

    private ArrayList<Food> foods;
    private FoodsPresenter presenter;
    private FoodsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foods);
        init();
    }

    private void init() {
        makePresenter();
        setAdapter();
        presenter.getFoods();
    }

    private void setAdapter() {
        RecyclerView recyclerView = findViewById(R.id.rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FoodsAdapter(this, foodsInterface);
        recyclerView.setAdapter(adapter);
    }

    private FoodsInterface foodsInterface = new FoodsInterface() {
        @Override
        public void delete(int index) {
            Toast.makeText(FoodsActivity.this, "This is Delete mode", Toast.LENGTH_LONG).show();
            foods.remove(index);
            adapter.setData(foods);
        }

        @Override
        public void itemClicked(Food food) {
            Toast.makeText(FoodsActivity.this, "This is Item mode", Toast.LENGTH_LONG).show();
        }
    };

    private void makePresenter() {
        presenter = new FoodsPresenter();
        presenter.attachView(this);
        presenter.start();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showData(ArrayList<Food> foods) {
        this.foods = foods;
        adapter.setData(foods);
    }

    @Override
    public void showErrorWithText(String text) {

    }
}
