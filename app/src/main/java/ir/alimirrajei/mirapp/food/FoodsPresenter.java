package ir.alimirrajei.mirapp.food;

import java.util.ArrayList;

public class FoodsPresenter implements FoodsContract.Presenter {

    private FoodsContract.View view;
    private Food food;

    @Override
    public void start() {

    }

    @Override
    public Food getSaveState() {
        return food;
    }

    @Override
    public void attachView(FoodsContract.View view) {
        this.view = view;
    }

    @Override
    public void onSaveState(Food model) {
        food = model;
    }


    @Override
    public void getFoods() {
        Food food0 = new Food(1, "Lavinia");
        Food food1 = new Food(2, "Cake");
        Food food2 = new Food(3, "Pie");
        Food food3 = new Food(4, "Steak");

        ArrayList<Food> foods = new ArrayList<>();
        foods.add(food0);
        foods.add(food1);
        foods.add(food2);
        foods.add(food3);
        view.showData(foods);
    }


}
