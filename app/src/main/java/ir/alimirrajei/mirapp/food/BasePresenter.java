package ir.alimirrajei.mirapp.food;

public interface BasePresenter<V, M>{
    void start();

    M getSaveState();

    void attachView(V view);

    void onSaveState(M model);

}
