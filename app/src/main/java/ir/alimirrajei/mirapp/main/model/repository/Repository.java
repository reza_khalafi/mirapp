package ir.alimirrajei.mirapp.main.model.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import ir.alimirrajei.mirapp.main.model.service.DataService;
import ir.alimirrajei.mirapp.main.model.service.RetrofitInstance;
import ir.alimirrajei.mirapp.main.model.entity.UserResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    private UserResponse userResponse;

    private MutableLiveData<UserResponse> userResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<UserResponse> getUserResponseMutableLiveData() {
        DataService dataService = RetrofitInstance.getService();
        Call<UserResponse> call = dataService.getUserResponse();
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    userResponse = response.body();
                    userResponseMutableLiveData.setValue(userResponse);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });

        return userResponseMutableLiveData;
    }
}
