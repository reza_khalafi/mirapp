package ir.alimirrajei.mirapp.main.model.service;

import ir.alimirrajei.mirapp.main.model.entity.UserResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface DataService {
    @GET("5d9728fc3b00001200c31472")
    Call<UserResponse> getUserResponse();
}
