package ir.alimirrajei.mirapp.main.model.service;

import ir.alimirrajei.mirapp.main.model.constant.MirConstants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    private static Retrofit retrofit = null;

    public static DataService getService() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(MirConstants.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit.create(DataService.class);
    }
}
