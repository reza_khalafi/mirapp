package ir.alimirrajei.mirapp.main.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import ir.alimirrajei.mirapp.main.model.entity.UserResponse;
import ir.alimirrajei.mirapp.main.model.repository.Repository;

public class UserViewModel extends ViewModel {
    private Repository repository;
    private LiveData<UserResponse> userResponseLiveData;

    public UserViewModel() {
        repository = new Repository();
    }

    public LiveData<UserResponse> getUserResponseLiveData() {
        return repository.getUserResponseMutableLiveData();
    }
}
