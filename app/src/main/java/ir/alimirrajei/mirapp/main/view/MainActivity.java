package ir.alimirrajei.mirapp.main.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import ir.alimirrajei.mirapp.R;
import ir.alimirrajei.mirapp.main.model.entity.UserResponse;
import ir.alimirrajei.mirapp.main.viewmodel.UserViewModel;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private UserViewModel viewModel;
    private UserResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        getUserResponse();
    }

    private void getUserResponse() {
        viewModel.getUserResponseLiveData().observe(this, userResponse -> {
            response = userResponse;
            Toast.makeText(this, response.getData().getUser().getLastName(), Toast.LENGTH_SHORT).show();
        });
    }
}
